FROM python:3.8

RUN ["apt-get", "update"]
RUN ["apt-get", "install", "-y", "ffmpeg"]
RUN ["pip3", "install", "pillow", "toml"]
RUN ["mkdir", "/streamer"]
WORKDIR /streamer

COPY streamer.py /streamer

CMD ["python3", "streamer.py"]
