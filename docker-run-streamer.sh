#!/bin/bash

IMAGE_NAME=music-streamer

docker run --rm -it -v $(pwd)/music:/streamer/music -v $(pwd)/static:/streamer/static $IMAGE_NAME

exit 0;
