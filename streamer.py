import os
import random
import itertools
import subprocess
from threading import Thread
from queue import Queue
from time import sleep

from PIL import Image, ImageDraw, ImageFont
import toml

# The name of the folder where the music files are contained.
MUSIC_FILES_FOLDER = 'music'
# The name of the folder where configuration, font, video and video playlist are stored
STATIC_FILES_FOLDER = 'static'

CONFIG_FILENAME = os.path.join(STATIC_FILES_FOLDER,"config.toml")
config = toml.load(CONFIG_FILENAME)
STREAM_URL = config['stream_url']
VIDEO_HORIZ_SIZE = config['video_horiz_size']
FFMPEG_PRESET = config['ffmpeg_preset']
FFMPEG_THREADS = config['ffmpeg_threads']
FFMPEG_CRF = config['ffmpeg_crf']

# The size of the text image in pixels.
# NOTE: probably should be as big as the video itself. Maybe get the background video size and assign it here.
TEXT_IMAGE_SIZE=(1920,200)

# Creates the text image as raw bytes that can be piped to ffmpeg.
def generate_text_image_bytes(txt):
    fontsize=24

    image = Image.new("RGBA", TEXT_IMAGE_SIZE, (255,255,255,0))#background color
    draw = ImageDraw.Draw(image)
    font = ImageFont.truetype(os.path.join(STATIC_FILES_FOLDER, "font.ttf"), fontsize)
    draw.text((0, 0), txt, (255,255,255), font=font)#text location,text color
    img_resized = image.resize(TEXT_IMAGE_SIZE, Image.ANTIALIAS)

    img_bytes = img_resized.tobytes()
    
    return img_bytes

def convert_music(filename):
    command = [
        'ffmpeg',
        '-i', filename,
        '-ar', '44100', '-ac', '2',
        "-acodec", "pcm_s16le",
        "-f", 's16le',
        "pipe:1",
    ]

    pipe = subprocess.run(command,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE,
                    bufsize=10**8)

    data = pipe.stdout

    return data


# The queue where the music objects will be put
music_queue = Queue(maxsize=1)
# This thread fetches music files and randomizes their order.
# Then it sends it to the queue.
# The music object is (filename, raw_music_binary, track_name_text_as_image_binary)
def _playlist_refresher_task():
    last_file = None
    while True:
        # Get the list of files in music folder.
        # NOTE: The music folder should not contain non-music files. TODO: filter out non-music files.
        music_files = os.listdir(MUSIC_FILES_FOLDER)

        #shuffling
        music_files = random.sample(music_files, len(music_files))

        # Prevent the repeating of tracks. 
        # In other words, detect if the previous playlist had the last song 
        # that is the same as the first song in the next playlist.
        # If so, move it to the end of the playlist.
        if music_files[0] == last_file:
            music_files = music_files[1:] + music_files[:1]

        # TODO: if I delete or rename a file, this thread will crash. Prevent that; allow hot modification.
        
        #Also, generate text images to show in the video.
        #list of tuples (filename, binary_sound, text_image_bytes)
        #binary_sound - in seconds
        #text_image_bytes - the title, will be passed to ffmpeg
        for f in music_files:
            music_queue.put((
                f,
                convert_music(os.path.join(MUSIC_FILES_FOLDER,f)),
                generate_text_image_bytes(os.path.splitext(f)[0]),
                ),)

            last_file = f

Thread(target=_playlist_refresher_task).start()

# The auxiliary playlist that will just loop the video infinitely.
VIDEO_PLAYLIST_FILENAME = os.path.join(STATIC_FILES_FOLDER,'infinite_video_playlist.txt')
# The pipe used to stream the music into ffmpeg.
AUDIO_PIPE = 'audio_pipe'
# The pipe used to stream the text image into ffmpeg.
TEXT_PIPE = 'text_pipe'
# Creating the pipes
os.mkfifo(AUDIO_PIPE)
os.mkfifo(TEXT_PIPE)

# The actual ffmpeg command to run
command = [
            # -re ensures the streaming is not done too fast
            'ffmpeg', 

            # video stream
            '-re', '-i', VIDEO_PLAYLIST_FILENAME, 

            # audio stream
            "-f", 's16le', # means 16bit input
           "-acodec", "pcm_s16le", # means raw 16bit input
           '-r', "44100", # the input will have 44100 Hz
           '-ac','2', # the input will have 2 channels (stereo)
            '-i', AUDIO_PIPE,

            # streaming text via pipe. Text location is in "overlay=x:y"
            '-f', 'rawvideo', '-pix_fmt', 'rgba', '-s', '{}x{}'.format(*TEXT_IMAGE_SIZE), '-i', TEXT_PIPE,

            # output settings. Not sure about all of them
            '-filter_complex', "[0]scale={VIDEO_HORIZ_SIZE}:-2[bg];".format(VIDEO_HORIZ_SIZE=VIDEO_HORIZ_SIZE) + "[bg][2:v] overlay=0:0", # Some codecs require the size of width and height to be a multiple of n. You can achieve this by setting the width or height to -n
            '-pix_fmt','yuvj420p','-x264-params','keyint=48:min-keyint=48:scenecut=-1','-b:v','4500k','-b:a','128k','-ar','44100','-acodec','aac','-vcodec','libx264',
            '-preset', FFMPEG_PRESET, '-crf', str(FFMPEG_CRF), '-threads', str(FFMPEG_THREADS),
            '-f', 'flv', STREAM_URL,
           ]
# Running ffmpeg
final_stream = subprocess.Popen(command, stdin=subprocess.PIPE)

class Streamer:

    def _audio_pipe_streamer(self):
        # Opening pipe for writing
        audio_pipe = os.open(AUDIO_PIPE, os.O_WRONLY)

        chunk_size = 1024

        while True:
            self.cur_data = music_queue.get()

            data = self.cur_data[1]
            for i in range(0, len(data), chunk_size):
                os.write(audio_pipe, data[i:chunk_size+i])

    def _text_pipe_streamer(self):
        # Opening pipe for writing
        text_pipe = os.open(TEXT_PIPE, os.O_WRONLY)

        while True:
            data = self.cur_data[2]
            os.write(text_pipe, data)

    def __init__(self):
        self.cur_data = ("",b'',b'')

        audio_thread = Thread(target=self._audio_pipe_streamer)  # thread1 writes samp1 to pipe1
        text_thread = Thread(target=self._text_pipe_streamer)  # thread2 writes samp2 to pipe2

        # Start the two threads
        audio_thread.start()
        text_thread.start()

        # Wait for the two writer threads to finish
        audio_thread.join()
        text_thread.join()

# Launching streaming threads
streamer = Streamer()

# TODO: come up with graceful shutdown, including all threads.

# Wait for FFmpeg sub-process to finish
final_stream.wait() 

os.close(AUDIO_PIPE)
os.close(TEXT_PIPE)
os.unlink(AUDIO_PIPE)
os.unlink(TEXT_PIPE)
