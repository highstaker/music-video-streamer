# Music video streamer

## Overview

This program lets you stream music together with video and the name of the song, just like many channels on Youtube do ([example](https://www.youtube.com/watch?v=5qap5aO4i9A)).

## Installation

This project was designed to be run in Docker, so obviously you should have Docker installed.

### Video

Put the video you want to play in a loop to `static` folder and call it `music_vid.mp4`.

Warning: at this point only this name is supported, as well as only 1920x1080 (other resolutions may misbehave), and only `mp4`. This may change in the future.

### Font

Put the font you want to use for the song name text to `static` and name it `font.ttf`. Make sure you're allowed to use the font (you own it or it is free to use).

Warning: only this name for a font is supported for now.

### Music

Create a `music` folder and put your music files in there.

Warning: no other files except for music should be there, or the program will crash. 

### Config

Rename `config.toml.sample` to `config.toml`.

Set the `stream_url` to whatever address and path you require.

## Deploy and run.

Run `create-image.sh` to create the image for the container.

Run `docker-run-streamer.sh` to start streaming.

To stop, simply use `Ctrl+C`. Warning: it is ugly right now, requires more work.
