#!/bin/bash

cd music || exit 1;

for file in *; do 
    if [ -f "$file" ]; then 
        echo "$file" 
        ffmpeg -i "$file" -ar 44100 -ac 2 converted_"$file"
        if [[ -f converted_"$file" ]]; then
            rm "$file"
            mv converted_"$file" "$file"
        fi
    fi 
done



exit 0;
